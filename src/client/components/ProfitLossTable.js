import React from 'react';
import _ from 'lodash';
import { blackScholes } from 'black-scholes';

const ProfitLossTable = ({ optionLegs, priceRange, nearestExpiryDate, riskFreeRate }) => {

    const maxDaysToCalculateFor = 10;
    const [daysUntilExpiration, setDaysUntilExpiration] = React.useState(0);

    React.useEffect(() => {
        const today = new Date();
        today.setHours(0, 0, 0, 0);
        let daysUntilExpiration = Math.ceil(Math.abs(new Date(nearestExpiryDate).getTime() - today.getTime()) / (1000 * 60 * 60 * 24)) + 1;
        if (daysUntilExpiration > maxDaysToCalculateFor) {
            daysUntilExpiration = maxDaysToCalculateFor;
        }
        setDaysUntilExpiration(daysUntilExpiration);
    }, [nearestExpiryDate]);

    return (
        optionLegs.length > 0 && <div>
            <table>
                <thead>
                    <tr>
                        <th>Price</th>
                        {_.range(daysUntilExpiration).map(day => {
                            const date = new Date();
                            date.setHours(0, 0, 0, 0);
                            date.setDate(date.getDate() + day);
                            return <th>{`${(date.getMonth() + 1).toString().padStart(2, 0)}-${date.getDate().toString().padStart(2, 0)}`}</th>;
                        })}
                    </tr>
                </thead>
                <tbody>
                    {_.range(priceRange.start, priceRange.end + 1).map(price => {
                        return (
                            <tr>
                                <td>{price.toFixed(2)}</td>
                                {_.range(daysUntilExpiration).map(day => {
                                    const date = new Date();
                                    date.setDate(date.getDate() + day);
                                    const aggregateValue = optionLegs.reduce((aggregator, optionLeg) => {
                                        const optionValue = blackScholes(price, optionLeg.strike, (daysUntilExpiration - day - 1) / 365, optionLeg.impliedVolatility, riskFreeRate / 100, optionLeg.type);
                                        if (optionLeg.position === 'long') {
                                            return aggregator + optionValue;
                                        } else {
                                            return aggregator - optionValue;
                                        }
                                    }, 0);
                                    return <td>{aggregateValue.toFixed(2)}</td>
                                })}
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );

}

export default ProfitLossTable;