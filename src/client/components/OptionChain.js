import React from 'react';

import OptionChainExpiryTable from './OptionChainExpiryTable';
import optionsHandler from '../handlers/options';

const OptionChain = ({ dispatch, symbol }) => {
    const [optionsChain, setOptionsChain] = React.useState('');

    React.useEffect(() => {
        const getOptionsChain = async (symbol) => {
            const optionsChain = await optionsHandler.getOptionChain(symbol);
            setOptionsChain(optionsChain);
            dispatch({ type: 'setOptionsChain', payload: optionsChain });
        }
        if (symbol) {
            getOptionsChain(symbol);
        }
    }, [dispatch, symbol]);

    const getOptionsChainTable = () => {
        if (optionsChain) {
            return optionsChain.array.map(options => {
                return <OptionChainExpiryTable dispatch={dispatch} options={options} symbol={symbol} />
            });
        }
    }

    return (
        <div>
            {getOptionsChainTable()}
        </div>
    );
}

export default OptionChain;