import React from 'react';

const PriceRange = ({ dispatch, currentStockPrice, priceRange }) => {

    const [{ start, end }, setPriceRange] = React.useState(priceRange);

    React.useEffect(() => {
        const trimmedPriceRange = { start: parseFloat(priceRange.start).toFixed(2), end: parseFloat(priceRange.end).toFixed(2) };
        setPriceRange(trimmedPriceRange); //Set default price range
    }, [priceRange]);

    const onSetPriceRangeClick = () => {
        dispatch({ type: 'setPriceRange', payload: { start: parseFloat(start), end: parseFloat(end) } });
    }

    const onPriceRangeChange = (e) => {
        const { value } = e.target;
        if (e.target.id === 'startPriceRange') {
            setPriceRange({ start: value, end });
        } else if (e.target.id === 'endPriceRange') {
            setPriceRange({ start, end: value })
        }
    }

    return (
        currentStockPrice > 0 && <div>
            <input id="startPriceRange" type="text" placeholder="start" value={start} onChange={onPriceRangeChange} />
            <input id="endPriceRange" type="text" placeholder="end" value={end} onChange={onPriceRangeChange} />
            <input type="button" value="Set Price Range" onClick={onSetPriceRangeClick} />
        </div>
    );

}

export default PriceRange;