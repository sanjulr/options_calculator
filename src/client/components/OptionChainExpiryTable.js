import React from 'react';

const OptionChainExpiryTable = ({ dispatch, symbol, options }) => {

    const [hidden, setHidden] = React.useState(true);

    const columnOrderFromLeftToRight = ['Change', 'Implied Volatility', 'Open Interest', 'Volume', 'Last Price', 'Bid', 'Ask'];

    const percentageValueColumns = ['Change', 'Implied Volatility'];

    const columnNameDataSourceMap = {
        'Change': 'change',
        'Implied Volatility': 'impliedVolatility',
        'Open Interest': 'openInterest',
        'Volume': 'volume',
        'Last Price': 'lastPrice',
        'Ask': 'ask',
        'Bid': 'bid',
        'Strike': 'strike',
    }

    const getOptionCode = (option, expiry, type) => {
        if (!option.strike) {
            return '-';
        }
        const optionExpiry = new Date(expiry);
        const code = `${symbol.toUpperCase()}${optionExpiry.getFullYear().toString().substring(2)}${(optionExpiry.getUTCMonth() + 1).toString().padStart(2, 0)}${optionExpiry.getUTCDate().toString().padStart(2, 0)}${type === 'call' ? 'C' : 'P'}${option.strike.toString().replace('.', '').padStart(5, 0)}`;
        return code;
    }

    const getOptionsRows = (options) => {
        const higherQuantityStrikeType = Object.keys(options.calls).length > Object.keys(options.puts).length ? options.calls : options.puts;
        const optionsRows = Object.keys(higherQuantityStrikeType).map(strike => {
            let call = options.calls[strike];
            let put = options.puts[strike];
            if (!call) {
                call = {};
            } if (!put) {
                put = {};
            }
            let currentOptionType = 'call';
            return <tr key={`${strike}`}>
                {[...columnOrderFromLeftToRight.slice().reverse(), 'Strike', ...columnOrderFromLeftToRight].map(column => {
                    const currentOption = currentOptionType === 'call' ? call : put;
                    if (column === 'Strike') {
                        currentOptionType = 'put';
                        return <td>{strike}</td>
                    }
                    else if (percentageValueColumns.indexOf(column) !== -1) {
                        let value = currentOption[columnNameDataSourceMap[column]];
                        if (!value) {
                            return <td>-</td>;
                        }
                        if (column === 'Implied Volatility') {
                            value = value * 100;
                        }
                        return <td>{`${parseFloat(value).toFixed(2)}%`}</td>
                    } else if (column === 'Bid' || column === 'Ask') {
                        if (!currentOption[columnNameDataSourceMap[column]]) {
                            return <td>-</td>;
                        }
                        return <td><input type="button" value={currentOption[columnNameDataSourceMap[column]]} option_type={currentOptionType} onClick={(e) => dispatch({ type: 'addOptionLeg', payload: { code: getOptionCode(currentOption, options.date, e.target.getAttribute('option_type')), strike, price: currentOption[columnNameDataSourceMap[column]], type: e.target.getAttribute('option_type'), position: column === 'Ask' ? 'long' : 'short', expiry: options.date, impliedVolatility: currentOption['impliedVolatility'] } })} /> </td>
                    }
                    else {
                        return <td>{currentOption[columnNameDataSourceMap[column]] || '-'}</td>
                    }
                })}
            </tr>
        });
        return optionsRows;
    }

    return (
        <div>
            <input type="button" value={new Date(options.date).toISOString().substring(0, 10)} onClick={() => setHidden(!hidden)} />
            <table hidden={hidden}>
                <thead>
                    <tr>
                        <th colSpan={columnOrderFromLeftToRight.length}>Calls</th>
                        <th colSpan={columnOrderFromLeftToRight.length}>Puts</th>
                    </tr>
                    <tr>
                        {columnOrderFromLeftToRight.map(column => <th>{column}</th>)}
                        <th>Strike</th>
                        {columnOrderFromLeftToRight.reverse().map(column => <th>{column}</th>)}
                    </tr>
                </thead>
                <tbody>
                    {getOptionsRows(options)}
                </tbody>
            </table>
        </div>
    );
}

export default OptionChainExpiryTable;