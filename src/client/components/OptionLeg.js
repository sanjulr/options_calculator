import React from 'react';

const OptionLeg = ({ dispatch, optionLeg }) => {
    return (
        <tr key={optionLeg.code}>
            <td>{optionLeg.position}</td>
            <td>{optionLeg.strike}</td>
            <td>{optionLeg.type}</td>
            <td>{optionLeg.price}</td>
            <td>{optionLeg.expiry.substring(0, 10)}</td>
            <td>{`${(optionLeg.impliedVolatility * 100).toFixed(2)}%`}</td>
            <td><input type="button" value="Remove" onClick={() => dispatch({ type: 'removeOptionLeg', payload: optionLeg.code })} /></td>
        </tr>
    );
}

export default OptionLeg;