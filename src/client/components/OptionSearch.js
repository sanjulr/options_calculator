import React from 'react';

import optionsHandler from '../handlers/options';

const OptionSearch = ({ dispatch }) => {
    const [symbol, setSymbol] = React.useState('');
    const [currentStockPrice, setCurrentStockPrice] = React.useState(0);

    const setSymbolAction = () => dispatch({ type: 'setSymbol', payload: symbol });

    const getCurrentStockPrice = async () => {
        const currentStockPrice = await optionsHandler.getCurrentStockPrice(symbol);
        const defaultPercentageRange = 2;
        const defaultPercentageRangeInPriceValue = currentStockPrice * (defaultPercentageRange / 100);
        setCurrentStockPrice(currentStockPrice);
        dispatch({ type: 'setCurrentStockPrice', payload: currentStockPrice });
        dispatch({ type: 'setPriceRange', payload: { start: parseFloat(currentStockPrice - defaultPercentageRangeInPriceValue), end: parseFloat(currentStockPrice + defaultPercentageRangeInPriceValue) } });
    }

    const search = () => {
        if (symbol) {
            setSymbolAction();
            getCurrentStockPrice();
        }
    }

    return (
        <div>
            <input type="text" id="symbol" placeholder="Symbol (Ex: SPY)" value={symbol} onChange={(e) => setSymbol(e.target.value)} />
            <input type="button" id="goButton" value="Search" onClick={search} />
            <span>Current price: {parseFloat(currentStockPrice).toFixed(2)}</span>
        </div>
    );
}

export default OptionSearch;