import React from 'react';

import OptionSearch from './OptionSearch';
import reducer from '../reducers/main';
import OptionChain from './OptionChain';
import SelectedOptionLegs from './SelectedOptionLegs';
import ProfitLossTable from './ProfitLossTable';
import PriceRange from './PriceRange';
import optionsHandler from '../handlers/options';

const Main = () => {
    const initialState = {
        symbol: undefined,
        optionsChain: {},
        currentStockPrice: 0,
        optionLegs: [],
        priceRange: { start: 0, end: 0 },
        nearestExpiryDate: undefined,
    };
    const [state, dispatch] = React.useReducer(reducer, initialState);
    const { symbol, optionLegs, currentStockPrice, priceRange, nearestExpiryDate } = state;
    const [riskFreeRate, setRiskFreeRate] = React.useState(0);

    React.useEffect(() => {
        const getRiskFreeRate = async () => {
            const todayRiskFreeRate = await optionsHandler.getRiskFreeRate();
            setRiskFreeRate(todayRiskFreeRate);
        }
        getRiskFreeRate();
    }, []);

    return (
        <div>
            <OptionSearch dispatch={dispatch} />
            <OptionChain dispatch={dispatch} symbol={symbol} />
            <SelectedOptionLegs dispatch={dispatch} optionLegs={optionLegs} />
            <PriceRange dispatch={dispatch} currentStockPrice={currentStockPrice} priceRange={priceRange} nearestExpiryDate={nearestExpiryDate} />
            <ProfitLossTable optionLegs={optionLegs} priceRange={priceRange} nearestExpiryDate={nearestExpiryDate} riskFreeRate={riskFreeRate} />
        </div >
    );
}

export default Main;