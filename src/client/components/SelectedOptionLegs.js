import React from 'react';

import OptionLeg from './OptionLeg';

const SelectedOptionLegs = ({ dispatch, optionLegs, nearestExpiryDate }) => {

    const totals = () => {
        const type = optionLegs.length > 1 ? 'multi leg' : 'single leg';
        const price = optionLegs.reduce((total, optionLeg) => {
            if (optionLeg.position === 'long') {
                return total + optionLeg.price;
            }
            return total - optionLeg.price;
        }, 0);
        const position = price > 0 ? 'debit' : 'credit';
        const strike = optionLegs.length > 1 ? '-' : optionLegs[0].strike;
        const expiry = nearestExpiryDate;
        const impliedVolatility = optionLegs.length > 1 ? '-' : optionLegs[0].impliedVolatility;
        return <tr>
            <td>{position}</td>
            <td>{strike}</td>
            <td>{type}</td>
            <td>{price.toFixed(2)}</td>
            <td>{expiry}</td>
            <td>{impliedVolatility}</td>
        </tr>
    }

    return (optionLegs.length > 0 &&
        <table>
            <thead>
                <tr>
                    <th>Position</th>
                    <th>Strike</th>
                    <th>Type</th>
                    <th>Price</th>
                    <th>Expiry</th>
                    <th>Implied Volatility</th>
                </tr>
            </thead>
            <tbody>
                {optionLegs.map(optionLeg => <OptionLeg dispatch={dispatch} optionLeg={optionLeg} />)}
                {totals()}
            </tbody>
        </table>
    );
}

export default SelectedOptionLegs;