import request from 'superagent';

import config from '../config';

const getOptionChain = async (symbol) => {
    const res = await request.get(`${config.getOptionChain}/${symbol}`);
    return res.body;
}

const getCurrentStockPrice = async (symbol) => {
    const res = await request.get(`${config.getCurrentStockPrice}/${symbol}`);
    if (!isNaN(res.text)) {
        return parseFloat(res.text);
    }
    return 0;
}

const getRiskFreeRate = async () => {
    const res = await request.get(config.getRiskFreeRate);
    if (!isNaN(res.text)) {
        return parseFloat(res.text);
    }
    return 0;
}

const functions = { getOptionChain, getCurrentStockPrice, getRiskFreeRate };

export default functions;