const getExpiryDateForSelectedOptionLegs = (optionLegs) => {
    if (optionLegs.length > 0) {
        let endDate;
        optionLegs.forEach(optionLeg => {
            if (!endDate) {
                endDate = optionLeg.expiry;
            }
            else if (optionLeg.expiry < endDate) {
                endDate = optionLeg.expiry;
            }
        });
        return endDate;
    }
}

const reducer = (state, action) => {
    switch (action.type) {
        case 'setSymbol':
            return { ...state, symbol: action.payload };
        case 'setOptionsChain':
            return { ...state, optionsChain: action.payload };
        case 'setCurrentStockPrice':
            return { ...state, currentStockPrice: action.payload };
        case 'addOptionLeg':
            state.optionLegs.push(action.payload);
            return { ...state, nearestExpiryDate: getExpiryDateForSelectedOptionLegs(state.optionLegs) };
        case 'removeOptionLeg':
            const indexOfOptionLegToRemove = state.optionLegs.findIndex(optionLeg => optionLeg.code === action.payload);
            if (indexOfOptionLegToRemove !== -1) {
                state.optionLegs.splice(indexOfOptionLegToRemove, 1);
            }
            return { ...state, nearestExpiryDate: getExpiryDateForSelectedOptionLegs(state.optionLegs) };
        case 'setPriceRange':
            return { ...state, priceRange: action.payload };
        default:
            throw new Error('Invalid action');
    }
}

export default reducer;