const endpoints = {
    getOptionChain: '/api/options/chain',
    getCurrentStockPrice: '/api/options/currentStockPrice',
    getRiskFreeRate: '/api/options/riskFreeRate'
}

const config = {
    ...endpoints,
}

export default config;