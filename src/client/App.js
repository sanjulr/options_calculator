import React from 'react';
import { Route, Switch } from 'react-router';

import Main from './components/Main';

const App = () => {
  return (
    <Switch>
      <Route path="/" component={Main} />
    </Switch>
  );
}

export default App;
