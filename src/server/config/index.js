const server = {
    port: 3000
}

const config = {
    ...server,
}

export default config;