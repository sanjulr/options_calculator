import { Router } from 'express';

import calculator from './calculator';

const router = Router();

router.get('/', (req, res) => {
    res.send('Hello Calculator');
    const symbol = 'FB';
    const expiry = new Date('2020-12-24');
    const contracts = [
        // { strike: 265, type: 'call', position: 'buy', cost: 15 },
        { strike: 280, type: 'call', position: 'buy', cost: 6.93 }
    ];
    calculator.getOptionChain(symbol, expiry).then(options => {
        contracts.forEach(async contract => {
            const optionType = contract.type;
            const strikePrice = contract.strike;
            const optionPrices = await calculator.getOptionPrice(symbol, strikePrice, optionType, expiry, options);
            optionPrices.forEach(optionPrice => {
                console.log({ stockPrice: optionPrice.stockPrice, strikePrice, optionType, ...optionPrice, 'p/l': (optionPrice.optionPrice - contract.cost) * 100 });
            });
        });
    });
});

export default router;