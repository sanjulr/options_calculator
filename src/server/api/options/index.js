import { Router } from 'express';

import options from './options';

const router = Router();

router.get('/chain/:symbol', async (req, res) => {
    const optionsChain = await options.getCompleteOptionsChain(req.params.symbol);
    res.status(200).json(optionsChain);
});

router.get('/currentStockPrice/:symbol', async (req, res) => {
    const currentStockPrice = await options.getStockPrice(req.params.symbol);
    res.status(200).send(`${currentStockPrice}`);
});

router.get('/riskFreeRate', async (req, res) => {
    const riskFreeRate = await options.getTodayRiskFreeRate();
    res.status(200).send(`${riskFreeRate}`);
});

export default router;