import algotrader from 'algotrader';
import request from 'superagent';
import parser from 'fast-xml-parser';
import blackScholes from 'black-scholes';

const getCompleteOptionsChain = async (symbol) => {
    const optionsData = await algotrader.Data.Yahoo.getOptionsChain(symbol);
    return optionsData;
}

const getOptionChainForExpiry = async (symbol, expiry) => {
    const optionsData = await getCompleteOptionsChain(symbol);
    const optionsChain = optionsData.array.find(date => {
        return expiry.toString() === date.date.toString();
    });
    return optionsChain;
}

const getTodayRiskFreeRate = async () => {
    const data = await request.get('https://www.treasury.gov/resource-center/data-chart-center/interest-rates/pages/XmlView.aspx?data=yield');
    const json = parser.parse(data.text);
    const riskFreeRate = json.pre.entry[json.pre.entry.length - 1].content['m:properties']['d:BC_1YEAR'];
    return riskFreeRate;
}

const getStockPrice = async (symbol) => {
    const data = await algotrader.Data.Yahoo.getQuotes(symbol, '1d', '1m', false);
    const quote = data[data.length - 1];
    return quote.price.close;
}

const getOptionPrice = async (symbol, strikePrice, optionType, expiryDate, options, stockPriceRange) => {
    const optionPrices = [];
    const option = options[`${optionType}s`][strikePrice];
    const riskFreeRate = (await getTodayRiskFreeRate()) / 100;
    const currentStockPrice = await getStockPrice(symbol);
    if (!stockPriceRange) {
        const range = 10;
        stockPriceRange = [Math.ceil(currentStockPrice) - range, Math.ceil(currentStockPrice) + range];
    }
    const today = new Date();
    const daysToExpiry = Math.ceil(Math.abs(expiryDate - today) / (1000 * 60 * 60 * 24));
    for (let i = 0; i <= daysToExpiry; i < i++) {
        const date = new Date();
        date.setDate(today.getDate() + i);
        for (let j = stockPriceRange[0]; j <= stockPriceRange[1]; j++) {
            const stockPrice = j;
            const optionPrice = blackScholes.blackScholes(stockPrice, strikePrice, (daysToExpiry - i) / 365, option.impliedVolatility, riskFreeRate, optionType);
            optionPrices.push({ date, stockPrice, optionPrice });
        }
    }
    return optionPrices;
}

const exportFunctions = { getCompleteOptionsChain, getOptionChainForExpiry, getOptionPrice, getTodayRiskFreeRate, getStockPrice };

export default exportFunctions;
