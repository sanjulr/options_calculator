import { Router } from 'express';

import options from './options';
import calculator from './calculator';

const router = Router();

router.use('/options', options);
router.use('/calculator', calculator);


export default router;