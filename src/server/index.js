import path from 'path';
import express from 'express';

import config from './config';
import render from './middleware/render';
import api from './api';

const app = express();

app.use('/dist', express.static(path.resolve(__dirname, './../client/assets/dist')));;
app.set('views', path.join(__dirname, './../client/assets'));

app.use('/api', api);

app.use('/', render);

app.listen(config.port, null, () => {
    console.log(`Server started at http://localhost:${config.port}`);
});
