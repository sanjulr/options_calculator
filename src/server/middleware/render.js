import React from 'react';
import ReactDOMServer from 'react-dom/server';
import { StaticRouter } from 'react-router';

import App from '../../client/App';

const render = (req, res, next) => {
    const location = `${req.baseUrl}${req.url}`;
    const content = ReactDOMServer.renderToString(
        <StaticRouter location={location}>
            <App />
        </StaticRouter>);
    res.status(200).render('index.ejs', { content });
    next();
}

export default render;