const path = require('path');

module.exports = (env, argv) => {
    const config = {
        entry: {
            web: './src/client',
        },
        output: {
            path: path.resolve(__dirname, './src/client/assets/dist/bundle'),
            filename: './[name].js',
            chunkFilename: '[id].chunk.js'
        },
        module: {
            rules: [
                {
                    test: /\.m?js$/,
                    exclude: /(node_modules)/,
                    loader: 'babel-loader',
                }
            ]
        },
        watch: true,
    };

    if (argv.mode === 'development') {
        config.devtool = 'cheap-module-source-map';
    }

    return config;
};